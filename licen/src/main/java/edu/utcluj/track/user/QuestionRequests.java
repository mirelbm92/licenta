package edu.utcluj.track.user;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class QuestionRequests {

    @Id
    @Column(name = "questionId")
    private int questionId;

    @Column(name = "question")
    private String question;

    @Column(name = "email")
    private String email;

    @Column(name = "answer")
    private String answer;



    public QuestionRequests(){

    }

    public QuestionRequests( String question, String email, String answer) {
        this.question = question;
        this.email = email;
        this.answer = answer;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public int getQuestionId() {
        return questionId;
    }

    public void setQuestionId(int questionId) {
        this.questionId = questionId;
    }
}
