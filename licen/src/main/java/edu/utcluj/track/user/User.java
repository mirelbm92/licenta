package edu.utcluj.track.user;

import javax.persistence.*;

@Entity
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "idUser")
    private int idUser;

    @Column(name = "name")
    private String name;

    @Column(name = "email")
    private String email;

    @Column (name = "password")
    private String password;

    @Column (name = "role")
    private String role;

    @Column (name = "phone")
    private String phone;

    @Column (name = "link")
    private String link;

    @Column (name = "path")
    private String path;

    @Column (name = "belongs")
    private int belongs;

    @Column (name = "applied")
    private boolean applied;

    @Column (name = "appliedTo")
    private int appliedTo;

    public User(){

    }

    public User(String email, String password, String role, String link, int belongs, String name, String path, String phone, boolean applied, int appliedTo) {
        this.email = email;
        this.password = password;
        this.role=role;
        this.link=link;
        this.belongs=belongs;
        this.name=name;
        this.path=path;
        this.phone=phone;
        this.applied = applied;
        this.appliedTo = appliedTo;
    }

    public User(String email) {
        this.email = email;
    }

    public int getIdUser() {return idUser;}

    public void setIdUser(int idUser){this.idUser=idUser;}

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public int getBelongs() {
        return belongs;
    }

    public void setBelongs(int belongs) {
        this.belongs = belongs;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isApplied() {
        return applied;
    }

    public void setApplied(boolean applied) {
        this.applied = applied;
    }

    public int getAppliedTo() {
        return appliedTo;
    }

    public void setAppliedTo(int appliedTo) {
        this.appliedTo = appliedTo;
    }
}
