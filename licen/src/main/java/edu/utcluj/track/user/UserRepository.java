package edu.utcluj.track.user;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

    @Query("SELECT u FROM User u WHERE LOWER(u.email) = LOWER(?1)")
    public User findByEmail(String email);

    @Query("SELECT o FROM User o WHERE LOWER(o.role) = LOWER(?1)")
    public List<User> findByRole(String role);

    @Query("SELECT o FROM User o WHERE LOWER(o.idUser) = LOWER(?1)")
    public User findById(int idUser);

    @Query("SELECT o FROM User o WHERE LOWER(o.idUser) = LOWER(?1)")
    List<User> findByMaster(int idUser);

    @Query("SELECT o FROM User o WHERE LOWER(o.idUser) = LOWER(?1)")
    User findByIntegerId(Integer id);
}
