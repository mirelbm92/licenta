package edu.utcluj.track.user;

import javax.persistence.*;

@Entity
public class Question {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "idQuestion")
    private int idUser;

    @Column(name = "question")
    private String question;

    @Column(name = "email")
    private String email;

    @Column (name = "answer")
    private String answer;


    public Question(){

    }

    public Question(String email, String question, String answer) {
        this.email = email;
        this.question = question;
        this.answer=answer;
    }

    public Question(String email) {
        this.email = email;
    }

    public int getIdUser() {return idUser;}

    public void setIdUser(int idUser){this.idUser=idUser;}

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }
}
