package edu.utcluj.track.user;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRequestRepository extends JpaRepository<UserRequests, Integer> {


    @Query("SELECT o FROM UserRequests o WHERE LOWER(o.requestedFrom) = LOWER(?1)")
    public UserRequests findById(int id);

    @Query("SELECT o FROM UserRequests o WHERE LOWER(o.requestedTo) = LOWER(?1)")
    public UserRequests findRequestById(int id);

    @Query("SELECT o FROM UserRequests o WHERE LOWER(o.requestedTo) = LOWER(?1)")
    public List<UserRequests> findByRequestId(int requestedTo);
}
