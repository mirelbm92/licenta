package edu.utcluj.track.user;

public class UserDto {

    private String name;
    private String email;
    private String password;
    private String role;
    private String phone;
    private String link;
    private String path;
    private int belongs;

    public UserDto(){

    }

    public UserDto(String email, String password, String role) {
        this.email = email;
        this.password = password;
        this.role=role;
    }

    public UserDto(String email, String password){
        this.email = email;
        this.password = password;
    }

    public UserDto(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public int getBelongs() {
        return belongs;
    }

    public void setBelongs(int belongs) {
        this.belongs = belongs;
    }
}
