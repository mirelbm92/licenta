package edu.utcluj.track.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.util.List;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserRequestRepository userRequestRepository;

    @Autowired
    private QuestionRequestRepository questionRequestRepository;

    public User loginUser(UserDto user) {
        if (userRepository.findByEmail(user.getEmail()) != null) {
            if (user.getPassword().equals(userRepository.findByEmail(user.getEmail())
                    .getPassword()))
                return userRepository.findByEmail(user.getEmail());
        }
        return null;
    }

    @Transactional
    public Boolean registerUser(UserDto user){
        User userReg = new User(user.getEmail(),user.getPassword(),user.getRole(),user.getLink(),user.getBelongs(),user.getName(),user.getPath(),user.getPhone(),false, 0);
        if ((userRepository.findByEmail(user.getEmail())) == null){
            userRepository.save(userReg);
            return true;
        }else{
            return false;
        }
    }

    @Transactional
    public Boolean updateAppliance(int id, int requestedTo, boolean applied){
        if (userRepository.findById(id) != null){
            User userUpd = userRepository.findById(id);
            userUpd.setApplied(applied);
            userUpd.setAppliedTo(requestedTo);
            userRepository.save(userUpd);
            return true;
        }else{
            return false;
        }
    }

    @Transactional
    public User validateEmail(UserDto user){
        if ((userRepository.findByEmail(user.getEmail())) != null)return userRepository.findByEmail(user.getEmail());
        else return null;
    }

    @Transactional
    public Boolean validateEmailBoolean(UserDto user){
        if ((userRepository.findByEmail(user.getEmail())) != null)return true;
        else return false;
    }

    public User verifyEmail(String email){
        return userRepository.findByEmail(email);
    }

    @Transactional
    public Boolean verifyPassword(UserDto user){
        if ((userRepository.findByEmail(user.getEmail()).getPassword()).equals(user.getPassword()))return true;
        else return true;
    }


    public List returnJSONArray(String role) {
        return userRepository.findByRole(role);
    }


    public boolean registerUserRequest(UserRequests request) {
        UserRequests userRequests = new UserRequests(request.getRequestedFrom(), request.getRequestedTo());
        if ((userRequestRepository.findById(request.getRequestedFrom())) == null) {
            User user = userRepository.findById(request.getRequestedTo());
            if(user != null) {
                if(user.getRole().equals("doctor")) {
                    userRequestRepository.save(userRequests);
                    return true;
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }else {
            return false;
        }
    }

    public boolean registerUserRequest(QuestionRequests request) {

        QuestionRequests questionRequest = new QuestionRequests(request.getQuestion(), request.getEmail(), request.getAnswer());

//        if ((questionRequestRepository.findQestionByid(request.getQuestionId())) == null) {
//            Question question = questionRequestRepository.findById(request.getRequestedTo());
//            if(user != null) {
//                if(user.getRole().equals("doctor")) {
//                    userRequestRepository.save(userRequests);
//                    return true;
//                }else{
//                    return false;
//                }
//            }else{
//                return false;
//            }
//        }else {
//            return false;
//        }
        return true;
    }

    public List getNotifications(int idUser) {
        return userRequestRepository.findByRequestId(idUser);
    }

    public boolean deleteUserRequest(UserRequests request) {

        UserRequests requestToDelete = userRequestRepository.findById(request.getRequestedFrom());
        if(requestToDelete != null){
            userRequestRepository.delete(requestToDelete);
            return true;
        }
        else{
            return false;
        }
    }

    public boolean acceptRequest(UserRequests request) {

        User user = userRepository.findById(request.getRequestedFrom());
        if(user != null) {
            user.setBelongs(request.getRequestedTo());
            userRepository.save(user);
            return true;
        }else{
            return false;
        }
    }

    public List getAllBelongingUsers(int id) {
        List<User> doctoranzi = userRepository.findByMaster(id);
        return doctoranzi;
    }

    public boolean deleteResident(Object masterId, Object id) {
//        User user = userRepository.findById(id);
        User user = userRepository.findById(Integer.parseInt(String.valueOf(id)));
        if(user != null) {
            if (user.getBelongs() == Integer.parseInt(String.valueOf(masterId))) {
                user.setBelongs(0);
                userRepository.save(user);
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

    public String getFilePath(String path, String fileName) {
        File folder=new File(path);
        File[] listOfFiles = folder.listFiles();
        for (int i = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].isFile()) {
                if(listOfFiles[i].getName().equals(fileName)) {
                    System.out.println("File " + listOfFiles[i].getName());
                    return path +"/"+ listOfFiles[i].getName();
                }
            } else if (listOfFiles[i].isDirectory()) {
                System.out.println("Directory " + listOfFiles[i].getName());
            }
        }
        return "";
    }

        public String getFileExtension(String fileName) {
            if(fileName.lastIndexOf(".") != -1 && fileName.lastIndexOf(".") != 0)
                return fileName.substring(fileName.lastIndexOf(".")+1);
            else return "";
        }

    public boolean upadePath(int id, String fullPath) {
        if (userRepository.findById(id) != null) {
           User userPathUpd = userRepository.findById(id);
           userPathUpd.setPath(fullPath);
           userRepository.save(userPathUpd);
        }
        return true;
    }
}
