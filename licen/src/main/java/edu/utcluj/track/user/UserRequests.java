package edu.utcluj.track.user;

import javax.persistence.*;

@Entity
public class UserRequests {

    @Id
    @Column(name = "requestedFrom")
    private int requestedFrom;

    @Column(name = "requestedTo")
    private int requestedTo;


    public UserRequests(){

    }

    public UserRequests(int requestedFrom,int requestedTo) {
        this.requestedTo=requestedTo;
        this.requestedFrom=requestedFrom;
    }

    public int getRequestedFrom() {
        return requestedFrom;
    }

    public void setRequestedFrom(int requestedFrom) {
        this.requestedFrom = requestedFrom;
    }

    public int getRequestedTo() {
        return requestedTo;
    }

    public void setRequestedTo(int requestedTo) {
        this.requestedTo = requestedTo;
    }
}
