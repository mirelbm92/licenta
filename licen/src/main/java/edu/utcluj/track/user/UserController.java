package edu.utcluj.track.user;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import edu.utcluj.track.utils.ResponseHelper;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

@RestController
@EnableAutoConfiguration
@RequestMapping(value="/user")
public class UserController {

    @Autowired
    UserService userService;

    @CrossOrigin
    @RequestMapping(value = { "/register" }, method = RequestMethod.POST)
    private ResponseEntity<JsonNode> registerUser(@RequestBody UserDto user) {
        ObjectMapper objectMapper = new ObjectMapper();
        HashMap<String, String> responseErrorJson = new HashMap<>();
        LinkedHashMap jsonToSend = new LinkedHashMap<String,User>();
        if(userService.registerUser(user)){

            jsonToSend.put("status","201");
            jsonToSend.put("message","User registred succesfully!");
            jsonToSend.put("user",user);

            JsonNode jsonNode = objectMapper.valueToTree(jsonToSend);
            return new ResponseEntity<JsonNode>(jsonNode,
                    HttpStatus.OK);
        }else{
            return ResponseHelper.setBadRequest("402","Cont deja existent!");
        }
    }

    @CrossOrigin
    @RequestMapping(value = { "/login" } , method = RequestMethod.POST)
    private ResponseEntity<JsonNode> login(@RequestBody UserDto user){
        ObjectMapper objectMapper = new ObjectMapper();
        UserDto userReceived = new UserDto();
        userReceived.setEmail(user.getEmail());
        userReceived.setPassword(user.getPassword());

        User userToCheck =  userService.validateEmail(userReceived);

        if(userToCheck != null) {
            if (userToCheck.getPassword().equals(userReceived.getPassword())) {
                LinkedHashMap jsonToSend = new LinkedHashMap<String, User>();

                jsonToSend.put("status", "201");
                jsonToSend.put("message", "User found successfully!");
                jsonToSend.put("user", userToCheck);
                jsonToSend.put("request", userService.getNotifications(userToCheck.getIdUser()));
                JsonNode jsonNode = objectMapper.valueToTree(jsonToSend);
                return new ResponseEntity<JsonNode>(jsonNode,
                        HttpStatus.OK);
            }else{
                return ResponseHelper.setBadRequest("402","Cont inexistent sau credentialele introduse sunt gresite.");
            }
        }else{
            return ResponseHelper.setBadRequest("402","Cont inexistent sau credentialele introduse sunt gresite.");
        }
    }

    @CrossOrigin
    @RequestMapping(value = { "/registerRequest" }, method = RequestMethod.POST)
    private ResponseEntity<JsonNode> registerUserRequest(@RequestBody UserRequests request) {
        if(userService.registerUserRequest(request) && userService.updateAppliance(request.getRequestedFrom(),request.getRequestedTo(),true)){
            return ResponseHelper.setGoodRequest("201","Inregistrare facuta cu sucec.Asteptati aprobarea.");
        }else {
            return ResponseHelper.setBadRequest("402","Deja ati facut o cerere de inregistrare.");
        }
    }

    @CrossOrigin
    @RequestMapping(value = { "/registerRequestAccept" }, method = RequestMethod.POST)
    private ResponseEntity<JsonNode> registerUserRequestAccept(@RequestBody UserRequests request) {
        ObjectMapper objectMapper = new ObjectMapper();

        if(userService.acceptRequest(request) && userService.deleteUserRequest(request)){
            LinkedHashMap jsonToSend = new LinkedHashMap<String, User>();

            jsonToSend.put("status", "201");
            jsonToSend.put("message", "User updated successfully!");
            jsonToSend.put("id", request.getRequestedFrom());

            JsonNode jsonNode = objectMapper.valueToTree(jsonToSend);
            return new ResponseEntity<JsonNode>(jsonNode,
                    HttpStatus.OK);
        }else{
            return ResponseHelper.setBadRequest("402","Something went horribly wrong!Please help :(");
        }
    }

    @CrossOrigin
    @RequestMapping(value = { "/registerRequestDeclined" }, method = RequestMethod.POST)
    private ResponseEntity<JsonNode> registerUserRequestDeclined(@RequestBody UserRequests request) {
        if(userService.deleteUserRequest(request) && userService.updateAppliance(request.getRequestedFrom(),0, false)){
            return ResponseHelper.setGoodRequest("201","Nu ati acceptat cererea de inregistrare.");
        }else{
            return ResponseHelper.setBadRequest("402","Something went horribly wrong!Please help :(");
        }
    }

    @CrossOrigin
    @RequestMapping(value = { "/getRegistredUser" }, method = RequestMethod.GET)
    private ResponseEntity registerUserRequestDeclined(@RequestHeader int id) {
        return ResponseEntity.ok(userService.getAllBelongingUsers(id));
    }

    @CrossOrigin
    @RequestMapping(value = { "/deleteRegistredUsers" }, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    private ResponseEntity<JsonNode> registerUserRequestDeclined(@RequestBody HashMap jsonObject) {

            if(userService.deleteResident(jsonObject.get("masterId"),jsonObject.get("residentId"))){
                return ResponseHelper.setGoodRequest("201","Userul a fost sters cu suces.");
            }

        return ResponseHelper.setBadRequest("402","Something went horribly wrong!Please help :(");
    }

    @CrossOrigin
    @RequestMapping(value = { "/return/jsonarray/doctor" } , method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    private ResponseEntity returnJson(){
        return ResponseEntity.ok(userService.returnJSONArray("doctor"));
    }

    @CrossOrigin
    @RequestMapping(value = { "/return/jsonarray/doctorand" } , method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    private ResponseEntity returnJsonDoctorand(){
        return ResponseEntity.ok(userService.returnJSONArray("doctorand"));
    }

    @CrossOrigin
    @RequestMapping(value = { "/loadFile" }, method = RequestMethod.GET)
    private String loadFile(@RequestHeader String fileName) throws IOException {
        String path="D:/repo";
        return userService.getFilePath(path,fileName);
    }

    @CrossOrigin
    @PostMapping("/fileUpload")
    public ResponseEntity<Object> fileUpload(@RequestParam("file") MultipartFile file,@RequestParam String doctorandId, String conducatorId)
            throws IOException {
        String parentPath="D:/repo";
        File directory=new File(parentPath);
        // Save file on system
        if (!file.getOriginalFilename().isEmpty()) {
//            FileUtils.cleanDirectory(directory);
            BufferedOutputStream outputStream = new BufferedOutputStream(
                    new FileOutputStream(
                            // new File("C:/xampp/htdocs/app2/app/images", file.getOriginalFilename())));
                            new File(parentPath, conducatorId + "." + userService.getFileExtension(file.getOriginalFilename()))));
            outputStream.write(file.getBytes());
            outputStream.flush();
            outputStream.close();
        }else{
            return new ResponseEntity<>("Invalid file.", HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>("Fisier updatat cu suces.",HttpStatus.OK);
    }

    @CrossOrigin
    @PostMapping("/imageUpload")
    public ResponseEntity<Object> imageUpload(@RequestParam("file") MultipartFile file,@RequestParam String id)
            throws IOException {
        String parentPath="D:/repo/images";
        File directory = new File(parentPath);
        // Save file on system
        if (!file.getOriginalFilename().isEmpty()) {
//            FileUtils.cleanDirectory(directory);
            BufferedOutputStream outputStream = new BufferedOutputStream(
                    new FileOutputStream
                            (new File(parentPath, id + "." + userService.getFileExtension(file.getOriginalFilename()))));
            outputStream.write(file.getBytes());
            outputStream.flush();
            outputStream.close();
            String fullPath = parentPath + "/" + file.getOriginalFilename();
            userService.upadePath(Integer.parseInt(id),fullPath);
            System.out.println(fullPath);
        }else{
            return new ResponseEntity<>("Fisier invalid sau acelas fisier exista deja.", HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>("Fisier updatat cu suces.",HttpStatus.OK);
    }

    @CrossOrigin
    @RequestMapping(value = { "/registerQuestion" }, method = RequestMethod.POST)
    private ResponseEntity<JsonNode> registerQuestion(@RequestBody QuestionRequests request) {
        if(userService.registerUserRequest(request)){
            return ResponseHelper.setGoodRequest("201","Request succesfully made");
        }else {
            return ResponseHelper.setBadRequest("402","Request faild to register or already exists!");
        }
    }

    @RequestMapping("/image/{personId}")
    @ResponseBody
    public HttpEntity<byte[]> getPhoto(@PathVariable String personId) {
        String parentPath="D:/repo/images";
        byte[] image = org.apache.commons.io.FileUtils.readFileToByteArray(new File(parentPath + File.separator + personId + ".png"));

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.IMAGE_PNG);
        headers.setContentLength(image.length);
        return new HttpEntity<byte[]>(image, headers);
    }
}
