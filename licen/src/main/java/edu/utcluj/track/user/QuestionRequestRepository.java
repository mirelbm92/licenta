package edu.utcluj.track.user;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface QuestionRequestRepository extends JpaRepository<QuestionRequests, Integer> {

    @Query("SELECT o FROM QuestionRequests o WHERE LOWER(o.questionId) = LOWER(?1)")
    public QuestionRequests findQestionByid(int questionId);

    @Query("SELECT o FROM QuestionRequests o WHERE LOWER(o.email) = LOWER(?1)")
    public QuestionRequests findQestionByEmail(String email);

    @Query("SELECT o FROM QuestionRequests o WHERE LOWER(o.email) = LOWER(?1)")
    public List<QuestionRequests> findQuestionByEmail(String email);
}
