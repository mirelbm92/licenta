package edu.utcluj.track.utils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import edu.utcluj.track.user.User;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.LinkedHashMap;

public class ResponseHelper {
    static ObjectMapper objectMapper = new ObjectMapper();

    public static ResponseEntity<JsonNode> setGoodRequest(String status, String message){
        LinkedHashMap jsonToSend = new LinkedHashMap<String, User>();
        jsonToSend.put("status",status);
        jsonToSend.put("message",message);
        JsonNode jsonNode = objectMapper.valueToTree(jsonToSend);
        return new ResponseEntity<JsonNode>(jsonNode,
                HttpStatus.OK);
    }

    public static ResponseEntity<JsonNode> setBadRequest(String status, String message){
        LinkedHashMap jsonToSend = new LinkedHashMap<String,User>();
        jsonToSend.put("status",status);
        jsonToSend.put("message",message);
        JsonNode jsonNode = objectMapper.valueToTree(jsonToSend);
        return new ResponseEntity<JsonNode>(jsonNode,
                HttpStatus.BAD_REQUEST);
    }
}
