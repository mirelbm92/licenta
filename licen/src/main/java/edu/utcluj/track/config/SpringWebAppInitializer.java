package edu.utcluj.track.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;


@Component
public class SpringWebAppInitializer implements WebApplicationInitializer {

    private static final Logger logger = LoggerFactory.getLogger(SpringWebAppInitializer.class);

    private static final String CONFIG_LOCATION = "edu.utcluj.track";
    private static final String MAPPING_URL = "/*";

    public void onStartup(ServletContext servletContext) throws ServletException {
        WebApplicationContext context = getContext();
        servletContext.addListener(new ContextLoaderListener(context));
        servletContext.addFilter("CORSFilter", CorsFilter.class).addMappingForUrlPatterns(null, true, "/*");
//        servletContext.addListener(new SessionListener());
        ServletRegistration.Dynamic dispatcher = servletContext.addServlet("DispatcherServlet",
                new DispatcherServlet(context));
        dispatcher.setLoadOnStartup(1);
        dispatcher.addMapping(MAPPING_URL);

        logger.debug("STARTING APPLICATION!!!");
    }

    private AnnotationConfigWebApplicationContext getContext() {
        AnnotationConfigWebApplicationContext context = new AnnotationConfigWebApplicationContext();
        context.setConfigLocation(CONFIG_LOCATION);
        context.register(WebConfig.class, PersistenceConfig.class);
        return context;
    }
}
